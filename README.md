## Usage

###### Dev server
```sh
$ npm run i (install)
$ npm run serve
```

###### Build
```sh
$ npm run build
```
---------

- build path: build/output