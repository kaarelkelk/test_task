({
    name: '../src/main',
    baseUrl: '../src',
    out: 'output/src/main.js',
    findNestedDependencies: true,
    mainConfigFile: '../src/main.js'
})
