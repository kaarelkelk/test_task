rm -rf output
node r.js -o build.js
node r.js -o build-css.js

cp -rf ../assets/img output/assets/img
cp -rf ../assets/fonts output/assets/fonts
cp ../index.html output/index.html
mkdir output/libs
mkdir output/libs/require
cp ./require.js output/libs/require/require.js

rm -f output/*.tmp
