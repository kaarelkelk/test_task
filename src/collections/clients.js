define([
  'jquery',
  'lodash',
  'backbone',
  'models/client'
], function($, _, Backbone, ClientModel){
  return Backbone.Collection.extend({

    model: ClientModel,
    url: GET_PATH('/persons/list:(id,name,org_name)?&user_id=2441197&' + API_TOKEN),
    parse: function(response) {
      return response.data;  
    }
  
  });
});
