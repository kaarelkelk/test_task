define([
  'jquery',
  'lodash',
  'backbone',
  'models/deal'
], function($, _, Backbone, DealModel){
  return Backbone.Collection.extend({

    model: DealModel,
    userId: null,

    initialize: function(id) {
      this.userId = id
    },

    url: function() {
      return GET_PATH('/persons/' + this.userId + '/deals/?status=all_not_deleted&' + API_TOKEN)
    },
    
    parse: function(response) {
      return response.data;  
    }
  
  });
});