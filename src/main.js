require.config({
  shim : {
      'bootstrap' : { 'deps' :['jquery'] }
  },
  paths: {
    // LIBS
    jquery: '../libs/jquery/jquery-min',
    bootstrap :  '../libs/bootstrap/bootstrap',
    underscore: '../libs/underscore/underscore-min',
    lodash: '../libs/lodash/lodash',
    backbone: '../libs/backbone/backbone-min',

    // PLUGINS
    text: '../libs/require/text',

    // TEMPLATE_PATH
    templates: '../assets/templates'
  }

});

var API_PATH = 'https://api.pipedrive.com/api/v1',
    API_TOKEN = 'api_token=5cf832ac56e25cee5efb8d84188445de1fa94fb8',
    GET_PATH = function(query) {
      return API_PATH + query
    }

require([
  'views/app',
  'router',
  'vm'
], function(AppView, Router, Vm){
  var appView = Vm.create({}, 'AppView', AppView);
  appView.render();
  Router.initialize({appView: appView});
});
