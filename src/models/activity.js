define([
  'jquery',
  'lodash',
  'backbone'
], function($, _, Backbone) {
  return Backbone.Model.extend({
    defaults: {
      id: null,
      company_id: null,
      user_id: null,
      done: false,
      type: '',
      reference_type: '',
      reference_id: null,
      due_date: '',
      due_time: '',
      duration: '',
      add_time: '',
      marked_as_done_time: '',
      subject: '',
      deal_id: null,
      org_id: null,
      person_id: null,
      active_flag: false,
      update_time: '',
      gcal_event_id: null,
      google_calendar_id: null,
      google_calendar_etag: null,
      note: '',
      person_name: '',
      org_name: '',
      deal_title: '',
      assigned_to_user_id: null,
      created_by_user_id: null,
      owner_name: '',
      person_dropbox_bcc: '',
      deal_dropbox_bcc: ''
    },

    url: function() {
      return GET_PATH('/activities/' + this.id + '?' + API_TOKEN)
    },

    initialize: function(data) {
      this.id = data.id
    },

    parse: function(response) {
      return response.data
    },
    toJSON: function () {
      var json = Backbone.Model.prototype.toJSON.call(this);
      json.date = this.formatDate(new Date(this.get('due_date')), this.get('due_time'));
      return json;
    },

    formatDate: function (date, time) {
      var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      var currentDate = new Date();
      
      if ( currentDate.getDate() + 1 === day ) return 'tomorrow ' + time
      if ( currentDate.getDate() - 1 === day ) return 'yesterday ' + time
      if ( currentDate.getDate() === day) return 'today ' + time 
      return day + ' ' + monthNames[monthIndex] + ' ' + year + ' ' + time;
    }
  });
});

