define([
  'jquery',
  'lodash',
  'backbone'
], function($, _, Backbone) {
  return Backbone.Model.extend({
    defaults: {
      id: null,
      org_name: '',
      name: ''
    },

    initialize: function(data) {
      this.id = data.id
    },

    url: function() {
      return GET_PATH('/persons/' + this.id + '/?status=all_not_deleted&' + API_TOKEN)
    },

    parse: function(response) {
      return response.data ? response.data : response
    }

  });
});