define([
  'jquery',
  'lodash',
  'backbone'
], function($, _, Backbone) {
  return Backbone.Model.extend({
    defaults: {
      id: null,
      name: '',
      value: ''
    }
  });
});