define([
  'jquery',
  'underscore',
  'backbone',
  'vm'
], function ($, _, Backbone, Vm) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Pages
      '': 'home',
      'client/:id': 'client',
      
      // Else
      '*actions': 'defaultAction'
    }
  });

  var initialize = function(options){
    var appView = options.appView;
    var router = new AppRouter(options);

    router.on('route:home', function () {
      require(['views/page/index'], function (BackbonePage) {
        var backbonePage = Vm.create(appView, 'BackbonePage', BackbonePage);
        backbonePage.render();

      });
    });

    router.on('route:client', function (id) {
      require(['views/page/client-details/client-details'], function (BackbonePage) {
        var backbonePage = Vm.create(appView, 'BackbonePage', BackbonePage, {id: id});
        backbonePage.render();
      });
    });

    router.on('route:defaultAction', function () {
      require(['views/page/index'], function (BackbonePage) {
        var backbonePage = Vm.create(appView, 'BackbonePage', BackbonePage);
        backbonePage.render();
      });
    });

    Backbone.history.start();
  };
  return { initialize: initialize };
});
