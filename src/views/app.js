define([
  'jquery',
  'lodash',
  'backbone',
  'vm',
  'events',
  'text!templates/layout.html'
], function($, _, Backbone, Vm, Events, layoutTemplate){
  var AppView = Backbone.View.extend({
    el: '.app',
    initialize: function () {},
    render: function () {
      var that = this;
      $(this.el).html(layoutTemplate);
      require(['views/header/menu', 'jquery', 'bootstrap'], function (HeaderMenuView) {
        var headerMenuView = Vm.create(that, 'HeaderMenuView', HeaderMenuView);
        headerMenuView.render();
      });
      require(['views/sidebar/sidebar'], function (SidebarView) {
        var sidebarView = Vm.create(that, 'SidebarView', SidebarView, {appView: that});
        sidebarView.render();
      });
    
    }
  });
  return AppView;
});
