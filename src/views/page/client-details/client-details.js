define([
  'jquery',
  'lodash',
  'backbone',
  'vm',
  'collections/deals',
  'models/client',
  'models/activity',
  'text!templates/widgets/client-information/client-information.html',
  'text!templates/widgets/client-information/deals.panel.html',
], function($, _, Backbone, Vm, 
            DealsCollection, ClientModel, ActivityModel, 
            ClientWidgetTemplate, ClientDealsTempalte ) {
  return Backbone.View.extend({
    el: '.page',

    // COLLECTIONS
    dealsCollection: DealsCollection,

    //MODELS
    client: ClientModel,
    next_activity: ActivityModel,
    last_activity: ActivityModel,

    initialize: function () {
      this.dealsCollection = new DealsCollection(this.id);
      this.client = new ClientModel({id: this.id});
      
      // FETCH CLIENT DETAILS
      this.client.fetch().then(
        this.renderClientDetails.bind(this),
        this.errorHandler
      );
    },

    renderClientDetails: function() {
      // RENDER CLIENT DETAILS TEMPLATE
      var _clientJSON = this.client.toJSON();
      this.$el.html(_.template(ClientWidgetTemplate)(_clientJSON));

      // FETCH LAST ACTIVITY
      if (_clientJSON.last_activity_id !== null) {
        this.last_activity = new ActivityModel({id: _clientJSON.last_activity_id})
        this.last_activity.fetch().then(
          this.renderClientLastActivity.bind(this),
          this.errorHandler
        );
      }

      // FETCH NEXT ACTIVITY
      if (_clientJSON.next_activity_id !== null) {
        this.next_activity = new ActivityModel({id: _clientJSON.next_activity_id})
        this.next_activity.fetch().then(
          this.renderClientNextActivity.bind(this),
          this.errorHandler
        );
      }
      
      // FETCH CLIENT DEALS
      this.dealsCollection.fetch().then(
        this.renderDeals.bind(this),
        this.errorHandler
      ) 
    },

    // RENDER NEXT ACTIVITY
    renderClientNextActivity: function() {
      var _next_activity = this.next_activity.toJSON();
      $('td.next_activity_field').html(_next_activity.subject + ', ' + _next_activity.date)
    },

    // RENDER LAST ACTIVITY
    renderClientLastActivity: function() {
      var _last_activity = this.last_activity.toJSON();
      $('td.last_activity_field').html(_last_activity.subject + ', ' + _last_activity.date)
    },

    // RENDER ALL CLIENT DEALS
    renderDeals: function() {
      var _el = $('.deals-panel');
          _el.html('')
      this.dealsCollection.each(function(item) {
        _el.append(_.template(ClientDealsTempalte)(item.toJSON()))
      })
      if (_el.html() === '') _el.append(_.template(ClientDealsTempalte)({
        title: 'No deals found',
        formatted_value: '...'
      }))
    },

    // RESPONSE ERROR HANDLER
    errorHandler: function(err) {
      console.error('e', err)
    }
  });
});
