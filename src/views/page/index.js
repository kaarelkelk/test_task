define([
  'jquery',
  'lodash',
  'backbone',
  'vm',
  'text!templates/page/index.html'
], function($, _, Backbone, Vm, backbonePageTemplate){
  var BackbonePage = Backbone.View.extend({
    el: '.page',

    render: function () {
      this.$el.html(backbonePageTemplate)
    }
  });
  return BackbonePage;
});
