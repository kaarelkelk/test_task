define([
  'jquery',
  'lodash',
  'backbone',
  'text!templates/sidebar/user-list-elem.html'
], function($, _, Backbone, UserListItemTemplate){
  var SidebarView = Backbone.View.extend({
    el: '.user-list',
    users: null,
    events: {
      'click a[user-link]' : 'handleClick'
    },
    render: function () {
      this.options.users.each(function (user, key) {
        $('.user-list').append(_.template(UserListItemTemplate)(user.toJSON()));
      })
      this.checkState();
    },

    // ON ITEM CLICK
    handleClick: function(e) {
      document.location.href = $(e.currentTarget).attr('user-link');
      this.checkState();
      return false;
    },

    // CHECK ACTIVE STATE
    checkState: function() {
      $('.user-list').children('a').each(function(i, el) {
        if ($(el).attr('user-link') === '#' + Backbone.history.getHash()) {
          $(el).children('li').addClass('active');
        } else {
          $(el).children('li').removeClass('active');
        }
      })
    }
  });

  return SidebarView;
});
