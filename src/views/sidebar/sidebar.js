define([
  'jquery',
  'lodash',
  'backbone',
  'vm',
  'text!templates/sidebar/sidebar.html',
  'collections/clients'
], function($, _, Backbone, Vm, sidebarTemplate, ClientsCollection){
  var SidebarView = Backbone.View.extend({
    el: '#sidebar',

    events: {
      'click [data-target=".sidebar-toggle"]' : 'sidebarToggle'
    },
    
    userList: [],

    initialize: function () {
      this.userList = new ClientsCollection();

      this.userList.fetch().then(
        this.renderClientLists.bind(this),
        this.errorHandler
      )

    },

    render: function () {
      var compiled = _.template(sidebarTemplate);
      this.$el.html(compiled({title: 'Peeter'}));
    },
    
    renderClientLists: function() {
      var that = this;
      require(['views/sidebar/items/user-list-item'], function (UserListItemView) {
        var userListItemView = Vm.create(that, 'UserListItemView', UserListItemView, {users: that.userList});
        userListItemView.render();
      });
    },

    sidebarToggle: function() {
      $('button[data-target="#sidebar"]').click();
    },

    errorHandler: function(err) {
      console.error('e', err)
    }
  });

  return SidebarView;
});
